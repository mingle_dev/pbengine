/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mingle.pbe;

/**
 *
 * @author chris.weaver
 */
public enum AgentState {
    Idle, Running, Failure, Paused, Interrupt
}
