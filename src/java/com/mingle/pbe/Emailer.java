/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mingle.pbe;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Properties;
import javax.activation.DataHandler;
import javax.activation.FileDataSource;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

/**
 *
 * @author chris.weaver
 */
public class Emailer {

    private String smtpServer;
    private List<String> to, cc, bcc, attachment;
    private String subject, message;
    private String fromAddress, fromName;

    /** Creates a new instance of Emailer */
    public Emailer(String smtpServer)
    {
        this.smtpServer = smtpServer;
        this.to = new ArrayList();
        this.cc = new ArrayList();
        this.bcc = new ArrayList();
        this.attachment = new ArrayList();
    }

    public void setSmtpServer(String smtpServer) {
        this.smtpServer = smtpServer;
    }

    public void setFromAddress(String fromAddress) {
        this.fromAddress = fromAddress;
    }

    public void setFromName(String fromName) {
        this.fromName = fromName;
    }

    public void addToAddress(String s) {
        if(s != null && s.length() > 0)
            this.to.add(s);
    }

    public void addCcAddress(String s) {
        if(s != null && s.length() > 0)
            this.cc.add(s);
    }

    public void addBccAddress(String s) {
        if(s != null && s.length() > 0)
            this.bcc.add(s);
    }

    public void addAttachment(String s) {
        if(s != null && s.length() > 0)
            this.attachment.add(s);
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public void appendMessage(String message) {
        this.message += message;
    }

    public String getMessage() {
        return message;
    }

    /**
     * sends an email message
     *
     * @param recipients String[]
     * @param subject String
     * @param message String
     * @param from String
     * @throws MessagingException
     */
    public void sendMessage(boolean debug)  throws Exception
    {
        //Set the host smtp address
        Properties props = new Properties();
        props.put("mail.smtp.host", smtpServer);

        // create some properties and get the default Session
        Session session = Session.getInstance(props, null);
        session.setDebug(debug);

        // create a message
        Message msg = new MimeMessage(session);
        msg.setHeader("Content-Type", "text/plain; charset=UTF-8");

        // set the from address
        if(fromName != null)
          msg.setFrom( new InternetAddress(this.fromAddress, this.fromName) );
        else
          msg.setFrom( new InternetAddress(this.fromAddress) );

        // set the to address(es)
        for (int i = 0; i < this.to.size(); i++)
            msg.addRecipient( Message.RecipientType.TO, new InternetAddress( this.to.get(i) ) );

        // set the cc address(es)
        for (int i = 0; i < this.cc.size(); i++)
            msg.addRecipient( Message.RecipientType.CC, new InternetAddress( this.cc.get(i) ) );

        // set the bcc address(es)
        for (int i = 0; i < this.bcc.size(); i++)
            msg.addRecipient( Message.RecipientType.BCC, new InternetAddress( this.bcc.get(i) ) );

        // Set the Subject
        msg.setSubject(this.subject);

        //create and fill the first message part
        MimeBodyPart messageBody = new MimeBodyPart();
        messageBody.setText(this.message);

        // create the Multipart and add its parts to it
        Multipart mp = new MimeMultipart();
        mp.addBodyPart(messageBody);

        if(attachment != null && attachment.size() > 0)
        {
            for(String file : attachment) {
                // create the attachment message part
                MimeBodyPart mbp2 = new MimeBodyPart();

                FileDataSource fds = new FileDataSource(file);
                mbp2.setDataHandler(new DataHandler(fds));
                mbp2.setFileName(fds.getName());

                mp.addBodyPart(mbp2);
            }
        }

        msg.setContent(mp);
        msg.setSentDate(new Date());

        Transport.send(msg);
    }
}