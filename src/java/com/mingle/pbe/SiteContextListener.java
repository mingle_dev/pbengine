/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mingle.pbe;

import com.mingle.pbe.beans.Agent;
import java.sql.Driver;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Enumeration;
import java.util.Random;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import org.apache.log4j.Logger;

/**
 *
 * @author chris.weaver
 */
public class SiteContextListener implements ServletContextListener
{
    private static Logger log = Logger.getLogger( SiteContextListener.class );

    private ScheduledExecutorService agentExecutor;
    private ScheduledExecutorService treeBuilderExecutor;
    private Agent[] agents;
    
    @Override
    public void contextInitialized(ServletContextEvent sce) {
        log.info("SiteContextListener Initializing");

        Random random = new Random( System.currentTimeMillis() );
        
        //@todo: if the enviornment.properties file fails to load, might as well stop
        
        int numAgents = 0; //(Integer) Environment.getProperty("QUEUE.NUMAGENTS");
        int initDelay = 2; //default start delay for agents
        int longDelay = 0; //(Integer) Environment.getProperty("QUEUE.CYCLEDELAY");
        int webTreeDelay = 60;
        
        try {
            numAgents = Integer.parseInt( String.valueOf( Environment.getProperty("QUEUE.NUMAGENTS") ) );
            longDelay = Integer.parseInt( String.valueOf( Environment.getProperty("QUEUE.CYCLEDELAY") ) );
            initDelay = longDelay;
            //configuring WebtreeBuilder 
            webTreeDelay = Integer.parseInt( String.valueOf( Environment.getProperty("WEBTREE.DELAY.MINS") ) );
        }catch(Exception ex) {
            log.error("Failed to set QUEUE environment variables");
        }
 
        //@todo: need a monitor to make sure things are running; start it first 
        //monitorExecutor = Executors.newSingleThreadScheduledExecutor();
        
        //startup agent threadpool
        agents = new Agent[numAgents];
        agentExecutor = Executors.newScheduledThreadPool(numAgents);

        for(int i=0; i<numAgents; i++) {
            agents[i] = new Agent(i);
            try { 
                initDelay = random.nextInt(10000) + 1;
                longDelay += (random.nextInt(5000) + 1); 
            }catch(Exception ex) { }
            
            log.info("Starting agent: " + i + " initDelay: " + initDelay + " longDelay: " + longDelay);
            
            agentExecutor.scheduleWithFixedDelay(new RenderAgent(agents[i], i), initDelay, longDelay, TimeUnit.MILLISECONDS);
        }

        //store agent array so that servlets can access
        ServletContext sc = sce.getServletContext();
        sc.setAttribute("agents", agents);        
        
        //startup treebuilder (web book browser)
        treeBuilderExecutor = Executors.newScheduledThreadPool(1);
        treeBuilderExecutor.scheduleWithFixedDelay(new WebtreeBuilder(), 1, webTreeDelay, TimeUnit.MINUTES);
        
        log.info("SiteContextListener Initialized with " + numAgents + " running");
    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {
        log.info("SiteContextListener Shutdown Initiated.");

        //shutdown any running agents
        for(int i=0; i<agents.length; i++) {        
            agents[i].interrupt();
        }
        
        //wait up to 60 seconds for 'running' agents to shutdown
        int i = 0;
        boolean idle = false;
        while(idle == false && i < 60) {
            try {
                Thread.sleep(1000);
            } catch (InterruptedException ignore) { }
            i++;

            idle = true;
            for(int j=0; j<agents.length; j++) {
                if(agents[j].getState() == AgentState.Running) {
                    log.info("Agent " + j + " in Running state");
                    idle = false;
                }
            }
        }        
        log.info("Agents offline. Begin scheduler shutdown.");
        
        try {
            agentExecutor.shutdownNow();
            if(agentExecutor.isShutdown())
                log.info("AgentScheduler shutdown successful.");
            else
                log.info("AgentScheduler failed to finish shutdown.");
        }catch(Exception ex) {}

        try {
            treeBuilderExecutor.shutdownNow();
            if(treeBuilderExecutor.isShutdown())
            if(agentExecutor.isShutdown())
                log.info("TreeBuilderExecutor shutdown successful.");
            else
                log.info("TreeBuilderExecutor failed to finish shutdown.");                
        }catch(Exception ex) {}
        
        //try {
        //    monitorExecutor.shutdownNow();
        //    //monitorExecutor.awaitTermination(15, TimeUnit.SECONDS);
        //    System.out.println("Monitor down: " + monitorExecutor.isShutdown());
        //}catch(Exception ex) {
        //    log.equals(ex);
        //}
        
        //de-register JDBC drivers
        Enumeration<Driver> drivers = DriverManager.getDrivers();
        while(drivers.hasMoreElements()) {
            Driver driver = drivers.nextElement();
            try {
                DriverManager.deregisterDriver(driver);
                log.info(String.format("deregistering jdbc driver: %s", driver));
            } catch (SQLException e) {
                log.error(String.format("Error deregistering driver %s", driver), e);
            }
        }
        
        log.info("SiteContextListener Shutdown Complete.");
    }
}