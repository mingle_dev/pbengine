/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mingle.pbe.controllers;

import com.mingle.pbe.AgentState;
import com.mingle.pbe.beans.Agent;
import com.mingle.pbe.sxapi.PricebookDAO;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.multiaction.MultiActionController;


/**
 *
 * @author chris.weaver
 */
//@Controller
public class QueueController extends MultiActionController
{
    public ModelAndView index(HttpServletRequest request, HttpServletResponse response) throws Exception 
    {
        ModelAndView model = new ModelAndView("index");

        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        model.addObject("msg", dateFormat.format(Calendar.getInstance().getTime()));

        PricebookDAO pbDao = new PricebookDAO();
        model.addObject("queuedBooks", pbDao.getQueuedBooks() );
        //pbDao.test();
        
        try {
            //Agent[] agents = (Agent[]) request.getServletContext().getAttribute("agents");
            ServletContext context = request.getSession().getServletContext();
            Agent[] agents = (Agent[]) context.getAttribute("agents");
            model.addObject("agents", agents);
        }catch(Exception ex) {
            System.out.println(ex);
        }
        
        return model;
    }    
    
    public ModelAndView pause(HttpServletRequest request, HttpServletResponse response) throws Exception 
    {
        try {
            int agent_id = Integer.parseInt(request.getParameter("agent_id"));
            ServletContext context = request.getSession().getServletContext();
            Agent[] agents = (Agent[]) context.getAttribute("agents");
            
            if(agents[agent_id].getState() == AgentState.Paused)
                agents[agent_id].resume();
            else 
                agents[agent_id].pause();
        }catch(Exception ex) {
            
        }
        
        //return index(request, response);
        return new ModelAndView("redirect:index.htm");
    }
    
    public ModelAndView kill(HttpServletRequest request, HttpServletResponse response) throws Exception 
    {
        request.getParameter("agent_id");
        
        ServletContext context = request.getSession().getServletContext();
        Agent[] agents = (Agent[]) context.getAttribute("agents");
        
        agents[0].kill();
        
        return index(request, response);
    }

    public ModelAndView json(HttpServletRequest request, HttpServletResponse response) throws Exception
    {
        ModelAndView model = new ModelAndView("rawText");
        
        try {
            ServletContext context = request.getSession().getServletContext();
            Agent[] agents = (Agent[]) context.getAttribute("agents");
            
            String data = "[";
            for(int i=0; i<agents.length; i++) {
                data += "{'id':" + agents[i].getId() + ", ";
                data += "{'state':" + agents[i].getState() + ", ";
                
            }
                        
            data += "]";
            
            model.addObject("content", data);
        }catch(Exception ex) {
            System.out.println(ex);
        }
        
        return model;
    }
}