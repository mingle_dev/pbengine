/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mingle.pbe.beans;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 *
 * @author chris.weaver
 */
public class Book implements Serializable
{
    private int PID;
    private static final long serialVersionUID = 1L;
    private String id, type, cono, custno, shipto, whse, emailaddr;
    private String bookName, custName;
    private boolean makePdf, delete;
    private Date queueTime;
    
    private List<Template> templates;
    
    public Book()
    {
        templates = new ArrayList<Template>();
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Book)) {
            return false;
        }
        Book other = (Book) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.mingle.pbe.entities.Book[id=" + id + "]";
    }

    /**
     * @return the name
     */
    public String getBookName() {
        return bookName;
    }

    /**
     * @param name the name to set
     */
    public void setBookName(String bookName) {
        this.bookName = bookName;
    }

    /**
     * @return the custno
     */
    public String getCustno() {
        return custno;
    }

    /**
     * @param custno the custno to set
     */
    public void setCustno(String custno) {
        this.custno = custno;
    }

    /**
     * @return the cono
     */
    public String getCono() {
        return cono;
    }

    /**
     * @param cono the cono to set
     */
    public void setCono(String cono) {
        this.cono = cono;
    }

    /**
     * @return the shipto
     */
    public String getShipto() {
        return shipto;
    }

    /**
     * @param shipto the shipto to set
     */
    public void setShipto(String shipto) {
        this.shipto = shipto;
    }

    /**
     * @return the whse
     */
    public String getWhse() {
        return whse;
    }

    /**
     * @param whse the whse to set
     */
    public void setWhse(String whse) {
        this.whse = whse;
    }

    /**
     * @return the emailaddr
     */
    public String getEmailaddr() {
        return emailaddr;
    }

    /**
     * @param emailaddr the emailaddr to set
     */
    public void setEmailaddr(String emailaddr) {
        this.emailaddr = emailaddr;
    }

    /**
     * @return the type
     */
    public String getType() {
        return type;
    }

    /**
     * @param type the type to set
     */
    public void setType(String type) {
        this.type = type;
    }

    /**
     * @return the makePdf
     */
    public boolean isMakePdf() {
        return makePdf;
    }

    /**
     * @param makePdf the makePdf to set
     */
    public void setMakePdf(boolean makePdf) {
        this.makePdf = makePdf;
    }

    /**
     * @return the delete
     */
    public boolean isDelete() {
        return delete;
    }

    /**
     * @param delete the delete to set
     */
    public void setDelete(boolean delete) {
        this.delete = delete;
    }

    /**
     * @return the custName
     */
    public String getCustName() {
        return custName;
    }

    /**
     * @param custName the custName to set
     */
    public void setCustName(String custName) {
        this.custName = custName;
    }

    /**
     * @return the templates
     */
    public List<Template> getTemplates() {
        return templates;
    }

    /**
     * @param templates the templates to set
     */
    public void setTemplates(List<Template> templates) {
        this.templates = templates;
    }
    
    public void addTemplate(Template template) {
        this.templates.add(template);
    }

    /**
     * @return the PID
     */
    public int getPID() {
        return PID;
    }

    /**
     * @param PID the PID to set
     */
    public void setPID(int PID) {
        this.PID = PID;
    }

    /**
     * @return the queueTime
     */
    public Date getQueueTime() {
        return queueTime;
    }

    /**
     * @param queueTime the queueTime to set
     */
    public void setQueueTime(Date queueTime) {
        this.queueTime = queueTime;
    }
}