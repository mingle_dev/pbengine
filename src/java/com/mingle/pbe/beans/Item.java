/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mingle.pbe.beans;

import java.math.BigDecimal;

/**
 *
 * @author chris.weaver
 */
public class Item
{
    public BigDecimal price, netAvail;
    public Integer pricingRecordNumber;
    public String unitsPerStockingText, status;  
}
