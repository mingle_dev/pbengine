/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mingle.pbe.beans;

import com.mingle.pbe.AgentState;
import java.util.Calendar;
import java.util.Date;

/**
 *
 * @author chris.weaver
 */
public class Agent
{
    private int id, numTemplates, currPos;
    private AgentState state;
    private Date startTm;
    private Date lastCheck;
    private Book book;
    private boolean isExit = false;
    
    public Agent() {}
    
    public Agent(int id)
    {
        this.id = id;
        this.state = AgentState.Idle;
    }
    
    public void interrupt()
    {
        isExit = true;
    }    
    
    public boolean isExit()
    {
        return isExit;
    }
    
    public void pause() {
        state = AgentState.Paused;
    }
    
    public void resume() {
        state = AgentState.Idle;
    }
    
    public void kill() {
        state = AgentState.Interrupt;
    }
    
    /**
     * @return the rowId
     */
    public int getId() {
        return id;
    }

    /**
     * @param rowId the rowId to set
     */
    public void setId(int id) {
        this.id = id;;
    }

    @Override
    public boolean equals(Object obj)
    {
        Agent other = (Agent) obj;
        
        if(this.id == other.id)
            return true;
        else
            return false;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 89 * hash + String.valueOf(this.id).hashCode();
        return hash;
    }

    /**
     * @return the status
     */
    public AgentState getState() {
        return state;
    }

    public String getPID() {
        return (book != null) ? String.valueOf(book.getPID()) : "";
    }
    
    public String getCustNo() {
        return (book != null) ? book.getCustno() : "";
    }
    
    public String getBookName() {
        return (book != null) ? book.getBookName() : "";
    }
    
    public void start(Book book) {
        this.startTm = Calendar.getInstance().getTime();
        this.book = book;
        this.state = AgentState.Running;
    }
    
    public void finish()
    {
        this.book = null;
        this.startTm = null;
        this.state = AgentState.Idle;
    }
    
    /**
     * @return the startTm
     */
    public Date getStartTm() {
        return startTm;
    }

    /**
     * @return the lastCheck
     */
    public Date getLastCheck() {
        return lastCheck;
    }

    /**
     * @param lastCheck the lastCheck to set
     */
    public void setLastCheck(Date lastCheck) {
        this.lastCheck = lastCheck;
    }

    /**
     * @return the numTemplates
     */
    public int getNumTemplates() {
        return numTemplates;
    }

    /**
     * @param numTemplates the numTemplates to set
     */
    public void setNumTemplates(int numTemplates) {
        this.numTemplates = numTemplates;
    }

    /**
     * @return the currTemplate
     */
    public int getCurrPos() {
        return currPos;
    }

    /**
     * @param currTemplate the currTemplate to set
     */
    public void setCurrPos(int currPos) {
        this.currPos = currPos;
    }
}