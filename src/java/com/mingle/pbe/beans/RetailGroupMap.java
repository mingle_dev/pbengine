/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mingle.pbe.beans;

import java.util.HashMap;
import java.util.List;

/**
 *
 * @author chris.weaver
 */
public class RetailGroupMap
{
    private HashMap<String, RetailGroup> map;

    public RetailGroupMap()
    {
        map = new HashMap<String, RetailGroup>();
    }

    public RetailGroupMap(List<RetailGroup> retailGroups)
    {
        map = new HashMap<String, RetailGroup>();
        
        for(RetailGroup g : retailGroups)
            map.put(g.getName(), g);
    }    
    
    public void add(RetailGroup itemGroup)
    {
        map.put(itemGroup.getName(), itemGroup);
    }

    public RetailGroup find(String prod)
    {
        for(int i=prod.length(); i>0; i--)
        {
            String p = prod.substring(0, i);

            if(map.containsKey(p))
                return map.get(p);
        }

        return null;
    }
}
