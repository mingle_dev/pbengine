/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mingle.pbe.beans;

/**
 *
 * @author chris.weaver
 */
public class RetailGroup
{
    private String id, name, descrip;
    private double gmPct, taxRate, commissionRate;
    private double laborCost, miscCost;

    public RetailGroup() {}
    
    public RetailGroup(String name)
    {
        this.name = name;
    }
    
    /**
     * @return the id
     */
    public String getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the gmPct
     */
    public double getGmPct() {
        return gmPct;
    }

    /**
     * @param gmPct the gmPct to set
     */
    public void setGmPct(double gmPct) {
        this.gmPct = gmPct;
    }

    /**
     * @return the addonAmt
     */
    public double getMiscCost() {
        return miscCost;
    }

    /**
     * @param addonAmt the addonAmt to set
     */
    public void setMiscCost(double miscCost) {
        this.miscCost = miscCost;
    }

    /**
     * @return the taxRate
     */
    public double getTaxRate() {
        return taxRate;
    }

    /**
     * @param taxRate the taxRate to set
     */
    public void setTaxRate(double taxRate) {
        this.taxRate = taxRate;
    }

    /**
     * @return the commissionRate
     */
    public double getCommissionRate() {
        return commissionRate;
    }

    /**
     * @param commissionRate the commissionRate to set
     */
    public void setCommissionRate(double commissionRate) {
        this.commissionRate = commissionRate;
    }

    /**
     * @return the laborCost
     */
    public double getLaborCost() {
        return laborCost;
    }

    /**
     * @param laborCost the laborCost to set
     */
    public void setLaborCost(double laborCost) {
        this.laborCost = laborCost;
    }

    /**
     * @return the descrip
     */
    public String getDescrip() {
        return descrip;
    }

    /**
     * @param descrip the descrip to set
     */
    public void setDescrip(String descrip) {
        this.descrip = descrip;
    }
    
    @Override
    public boolean equals(Object obj)
    {
        if(obj instanceof RetailGroup == false)
            return false;

        if(this.getName().equalsIgnoreCase( ((RetailGroup) obj).getName() ))
            return true;
        else 
            return false;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 73 * hash + (this.name != null ? this.name.hashCode() : 0);
        return hash;
    }
}
