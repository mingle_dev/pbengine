/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mingle.pbe;

/**
 *
 * @author chris.weaver
 */
public class Utilities
{
    /**
     * 
     * @param col
     * @return 
     */
    public static int getColPosition(String col)
    {
        //@todo; add recursion for multiple chars
        
        if(col.equalsIgnoreCase(("A"))) return 0;
        if(col.equalsIgnoreCase(("B"))) return 1;
        if(col.equalsIgnoreCase(("C"))) return 2;
        if(col.equalsIgnoreCase(("D"))) return 3;
        if(col.equalsIgnoreCase(("E"))) return 4;
        if(col.equalsIgnoreCase(("F"))) return 5;
        if(col.equalsIgnoreCase(("G"))) return 6;
        if(col.equalsIgnoreCase(("H"))) return 7;
        if(col.equalsIgnoreCase(("I"))) return 8;
        if(col.equalsIgnoreCase(("J"))) return 9;
        if(col.equalsIgnoreCase(("K"))) return 10;
        if(col.equalsIgnoreCase(("L"))) return 11;
        if(col.equalsIgnoreCase(("M"))) return 12;
        if(col.equalsIgnoreCase(("N"))) return 13;
        if(col.equalsIgnoreCase(("O"))) return 14;
        if(col.equalsIgnoreCase(("P"))) return 15;
        if(col.equalsIgnoreCase(("Q"))) return 16;
        if(col.equalsIgnoreCase(("R"))) return 17;
        if(col.equalsIgnoreCase(("S"))) return 18;
        if(col.equalsIgnoreCase(("T"))) return 19;
        if(col.equalsIgnoreCase(("U"))) return 20;
        if(col.equalsIgnoreCase(("V"))) return 21;
        if(col.equalsIgnoreCase(("W"))) return 22;
        if(col.equalsIgnoreCase(("X"))) return 23;
        if(col.equalsIgnoreCase(("Y"))) return 24;
        if(col.equalsIgnoreCase(("Z"))) return 25;
        
        return 0;
    }    
}
