/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mingle.pbe;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.AbstractController;

/**
 *
 * @author chris.weaver
 */
public class IndexController extends AbstractController
{
    private static final Logger log = Logger.getLogger( IndexController.class );

    public IndexController() {
    }
    
    @Override
    protected ModelAndView handleRequestInternal(HttpServletRequest request, HttpServletResponse response) throws Exception
    {
        return new ModelAndView("redirect:queue/index.htm");
    }
}