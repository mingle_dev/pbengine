/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mingle.pbe.sxapi;

import com.infor.sxapi2.proxy.ApiAppObj;
import java.io.Serializable;
import org.apache.log4j.Logger;

/**
 *
 * @author chris.weaver
 */
public class SxAPIWorker implements Serializable
{
    /* SxAPI AppServer Connection Handler */
    private ApiAppObj apiAppObj;

    /* SxAPI Application Server */
    protected String apiAppSvrUrl;

    /* SxAPI apiAppSvrRetryConnectTimes */
    private int apiAppSrvRetries;
    
    /* SxAPI apiAppSvrRetryConnectSleepSeconds */
    private int apiAppsrvSleep;
    
    /* SxAPI Connection identifier */
    protected String apiConnId;

    /* Log4j */
    private static Logger log4j = Logger.getLogger( SxAPIWorker.class );

    public SxAPIWorker() {
    }

    public SxAPIWorker(String apiAppSvrUrl, int apiAppSrvRetries, int apiAppsrvSleep)
    {
        this.apiAppSvrUrl = apiAppSvrUrl;
        this.apiAppSrvRetries = apiAppSrvRetries;
        this.apiAppsrvSleep = apiAppsrvSleep;
    }

    /**
     * @return the sxapiWorker
     */
    protected ApiAppObj get()
    {
        try {
            if(apiAppObj == null)
                apiAppObj = new ApiAppObj(apiAppSvrUrl, "", "", "", apiAppSrvRetries, apiAppsrvSleep);
        }catch(Exception ex) {
            log4j.error("Couldn't create ApiAppObj for SxapiWorker.");
            log4j.error(apiAppSvrUrl);
            log4j.error(ex);
            throw new RuntimeException("Couldn't create ApiAppObj for SxapiWorker", ex);
        }

        return apiAppObj;
    }
}