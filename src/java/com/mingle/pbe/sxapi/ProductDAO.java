/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mingle.pbe.sxapi;

import java.math.BigDecimal;
import java.util.HashMap;

import com.infor.sxapi2.paramclass.ICGetWhseProductDataGeneralV2.input.ICGetWhseProductDataGeneralV2Request;
import com.infor.sxapi2.paramclass.ICGetWhseProductDataGeneralV2.output.ICGetWhseProductDataGeneralV2Response;
import com.infor.sxapi2.paramclass.OEPricingV4.input.OEPricingV4Request;
import com.infor.sxapi2.paramclass.OEPricingV4.output.OEPricingV4Response;
import com.infor.sxapi2.services.callobject.ICGetWhseProductDataGeneralV2;
import com.infor.sxapi2.services.callobject.OEPricingV4;
import com.mingle.pbe.Environment;
import com.mingle.pbe.beans.Item;

/**
 *
 * @author chris.weaver
 */
public class ProductDAO
{
    SxAPIWorker sxAPIWorker;
    
    private HashMap<String, Item> products = null;
    private int cacheHits;

    public ProductDAO(SxAPIWorker sxAPIWorker)
    {
        this.sxAPIWorker = sxAPIWorker;
        products = new HashMap<String, Item>();
        cacheHits = 0;
    }
    
    /**
     * 
     * @param cono
     * @param custno
     * @param shipto
     * @param prod
     * @param whse
     * @return
     * @throws Exception 
     */
    public synchronized Item getItemPrice(String cono, String custno, String shipto, String prod, String whse) throws Exception
    {
        //in cache
        if(products.containsKey(prod)) {
            cacheHits++;
            return products.get(prod);
        }        
        
        Item item = new Item();
        
        if(cono == null || custno == null || prod == null || whse == null)
            throw new Exception("cono, custno, prod, whse are required");

        //verify item is actually good in both icsp and icsw for the requesting whse
        try {
            ICGetWhseProductDataGeneralV2Request req = new ICGetWhseProductDataGeneralV2Request();
            req.setProduct(prod);
            req.setWhse(whse);

            ICGetWhseProductDataGeneralV2 obj = new ICGetWhseProductDataGeneralV2();
            ICGetWhseProductDataGeneralV2Response res = obj.prepareResults(sxAPIWorker.get(), "CONO=" + cono + "|OPER=" + Environment.getProperty("SXAPI.OPER"), "", req);

            item.status = res.getStatusType();

            if(!res.getErrorMessage().isEmpty())
                throw new Exception(res.getErrorMessage());
            
        }catch(Exception ex) {
            throw new Exception(ex.getMessage());
        }
        
        //get the item price
        try {
            OEPricingV4Request req = new OEPricingV4Request();
            req.setCustomerNumber(new BigDecimal(custno));
            req.setShipTo( (shipto == null) ? "" : shipto );
            req.setProductCode(prod);
            req.setWarehouse(whse);

            OEPricingV4 obj = new OEPricingV4();
            OEPricingV4Response res = obj.prepareResults(sxAPIWorker.get(), "CONO=" + cono + "|OPER=" + Environment.getProperty("SXAPI.OPER"), "", req);

            item.price = res.getPrice();
            item.netAvail = res.getNetAvailable();
            item.pricingRecordNumber = res.getPricingRecordNumber();
            item.unitsPerStockingText = res.getUnitsPerStockingText();

            if(!res.getErrorMessage().isEmpty())
                throw new Exception(res.getErrorMessage());
            
        }catch(Exception ex) {
            throw new Exception(ex.getMessage());
        }
        
        products.put(prod, item);
        
        return item;
    } 
    
    public synchronized boolean flushCache()
    {
        products.clear();
        return products.isEmpty();
    } 
    
    public synchronized int getCacheHits()
    {
        return cacheHits;
    }
}