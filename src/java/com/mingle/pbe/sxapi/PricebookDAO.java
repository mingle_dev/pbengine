/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mingle.pbe.sxapi;

import java.io.FileOutputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.mingle.pbe.Environment;
import com.mingle.pbe.beans.Book;
import com.mingle.pbe.beans.RetailGroup;
import com.mingle.pbe.beans.Template;

/**
 *
 * @author chris.weaver
 */
public class PricebookDAO
{
    private Connection conn = null;
    private static final Logger log = Logger.getLogger( PricebookDAO.class );
    private final String workPath = String.valueOf(Environment.getProperty("workPath"));
        
    public PricebookDAO()
    {
        try {
            Class.forName(String.valueOf(Environment.getProperty("SQL.DRIVER")));
            conn = DriverManager.getConnection(String.valueOf(Environment.getProperty("SQL.URL")), 
                                               String.valueOf(Environment.getProperty("SQL.USER")), 
                                               String.valueOf(Environment.getProperty("SQL.PASS")));
        }catch(Exception ex) {
            log.error("Unable to establish Connection. " + ex.getMessage());
        }        
    }
    
    public synchronized Book getQueuedBook() throws SQLException
    {
        Book book = null;
        
        if(conn == null)
            throw new SQLException("java.sql.SQLException is NULL");

        PreparedStatement stmt = null;
        ResultSet rs = null;

        //get book id from FIFO queue
        String SPsql = "EXEC hive.dbo.usp_dequeuePricebookFifoQueue";
        stmt = conn.prepareStatement(SPsql);
        rs = stmt.executeQuery();
                
        if(rs.next()) {
            book = new Book();
            book.setPID(rs.getInt("id"));
            book.setId(rs.getString("pricebook_id"));
        }else{
            return null;
        }

        //update queued book with pid and set to run
        stmt = conn.prepareStatement("UPDATE hive.dbo.pricebooks SET pid=?, status='Running' WHERE id=?");
        stmt.setInt(1, book.getPID());
        stmt.setString(2, book.getId());
        stmt.executeUpdate();

        //get book information
        stmt = conn.prepareStatement(
                "SELECT p.id, a.cono, a.custno, a.territory, p.name as 'book_name', p.type, a.name as 'cust_name', p.ship_to, p.emailaddr, " +
                "  p.makepdf, p.remove " +
                "FROM hive.dbo.pricebooks p " +
                "JOIN hive.dbo.view_accounts a ON (a.id = p.account_id) " +
                "WHERE p.pid=?");
        stmt.setInt(1, book.getPID());
        
        rs = stmt.executeQuery();
        while(rs.next()) {
            book.setType(rs.getString("type"));
            book.setCono(rs.getString("cono"));
            book.setCustno(rs.getString("custno"));
            book.setShipto(rs.getString("ship_to"));
            book.setWhse(rs.getString("territory"));
            book.setEmailaddr(rs.getString("emailaddr"));
            book.setDelete(rs.getBoolean("remove"));
            book.setMakePdf(rs.getBoolean("makepdf"));
            book.setBookName(rs.getString("book_name"));
            book.setCustName(rs.getString("cust_name"));
        }        

        if(book == null) 
            return null;
        
        //get templates
        stmt = conn.prepareStatement(
                "SELECT pt.name, ppt.template_id AS id " + //, file_data, test_file " +
                "FROM hive.dbo.pricebooks_pricebookTemplates ppt " + 
                "JOIN hive.dbo.pricebookTemplates pt ON (pt.id = ppt.template_id) " + 
                "WHERE ppt.pricebook_id=? and pt.deleted=0 AND pt.status='Active' " + 
                "ORDER BY pt.sort_order");
        stmt.setString(1, book.getId());
        
        //rs = stmt.executeQuery();
        rs = stmt.executeQuery();
        while(rs.next()) {
            Template t = new Template();
            t.setId(rs.getString("id"));
            t.setName(rs.getString("name"));
            book.addTemplate(t);
        }

        rs.close();
        stmt.close();

        return book;
    }    
    
    public synchronized boolean loadTemplates(Book book) throws SQLException
    {
        if(conn == null)
            throw new SQLException("java.sql.SQLException is NULL");

        PreparedStatement stmt = null;
        ResultSet rs = null;

        stmt = conn.prepareStatement(
                "SELECT ppt.template_id AS id, pt.name, test_file " +
                "FROM hive.dbo.pricebooks_pricebookTemplates ppt " + 
                "JOIN hive.dbo.pricebookTemplates pt ON (pt.id = ppt.template_id) " + 
                "WHERE ppt.pricebook_id=? and pt.deleted=0 AND pt.status='Active' " + 
                "ORDER BY pt.sort_order");
        stmt.setString(1, book.getId());
      
        rs = stmt.executeQuery();
        while(rs.next()) {
            try {
                if(rs.getString("test_file") != null && !rs.getString("test_file").isEmpty()) {
                    //save file to local fs
                    FileOutputStream fos = new FileOutputStream(workPath + book.getPID() + "_" + rs.getString("id") + ".xls");
                    byte[] fileData = hexStringToByteArray(rs.getString("test_file"));
                    fos.write( fileData );
                    fos.close();
                }else{
                    log.error(rs.getString("name") + " is emtpy or null.");
                }
            }catch(Exception ex) {
                log.error(ex + " " + rs.getString("name"));
                return false;
            }        
        }
        
        rs.close();
        stmt.close();        
        
        return true;
    }

    public List<Template> loadWebTreeTemplates() throws SQLException
    {
        if(conn == null)
            throw new SQLException("java.sql.SQLException is NULL");

        List<Template> l = new ArrayList<Template>();

        PreparedStatement stmt = null;
        ResultSet rs = null;

        stmt = conn.prepareStatement("SELECT id, name, test_file FROM hive.dbo.pricebookTemplates WHERE webtree=1 ");
        rs = stmt.executeQuery();
        while(rs.next()) {
            try {
                if(rs.getString("test_file") != null && !rs.getString("test_file").isEmpty()) {
                    //setup id,name
                    Template t = new Template();
                    t.setId(rs.getString("id"));
                    t.setName(rs.getString("name"));                    
                    
                    //save file to local fs
                    FileOutputStream fos = new FileOutputStream(workPath + rs.getString("id") + ".xls");
                    byte[] fileData = hexStringToByteArray(rs.getString("test_file"));
                    fos.write( fileData );
                    fos.close();
            
                    //add to array
                    l.add(t);
                }else{
                    log.error(rs.getString("name") + " is emtpy or null.");
                }
            }catch(Exception ex) {
                log.error(ex + " " + rs.getString("name"));
            }
        }
        rs.close();
        stmt.close();

        return l;
    }    
    
    public void clearWebTree() throws SQLException
    {
        if(conn == null)
            throw new SQLException("java.sql.SQLException is NULL");

        PreparedStatement stmt = null;
        //stmt = conn.prepareStatement("TRUNCATE TABLE productPricebookItems"); //don't have permissions
        stmt = conn.prepareStatement("DELETE FROM hive.dbo.productPricebookItems");
        stmt.execute();
        stmt.close();
    }

    public void insertWebTreeItem(String[] item) throws SQLException
    {
        if(conn == null)
            throw new SQLException("java.sql.SQLException is NULL");

        PreparedStatement stmt = null;
        stmt = conn.prepareStatement("INSERT INTO hive.dbo.productPricebookItems VALUES (?,?,?,?,?)");
        stmt.setString(1, item[0]); //template_id
        stmt.setString(2, item[1]); //template_name
        stmt.setString(3, item[2]); //sheet_name
        stmt.setString(4, item[3]); //prod
       
        //sort_order
        int sort = 9999;
        if(item[4] != null)
            try { 
                sort = Integer.parseInt(item[4]); 
            }catch(Exception ig) {}
        stmt.setInt(5, sort);         
        
        stmt.executeUpdate();
        stmt.close();
    }    
 
    public List<Book> getQueuedBooks() throws SQLException
    {
        List<Book> bookList = new ArrayList<Book>();
        
        if(conn == null)
            throw new SQLException("java.sql.SQLException is NULL");

        PreparedStatement stmt = null;
        ResultSet rs = null;

        //get book information
        stmt = conn.prepareStatement(
                "SELECT p.id, a.cono, a.custno, a.territory, p.name as 'book_name', p.type as 'book_type', a.name as 'cust_name', p.ship_to, p.emailaddr, " +
                "  p.makepdf, p.remove, p.updated " +
                "FROM hive.dbo.pricebookFifoQueue f " +
                "JOIN hive.dbo.pricebooks p ON (p.id = f.pricebook_id) " +
                "JOIN hive.dbo.view_accounts a ON (a.id = p.account_id) " +
                //"WHERE p.type='dealer' AND p.status='Queued' AND p.pid IS NULL AND p.deleted = 0 " + 
                "WHERE p.pid IS NULL AND p.deleted = 0 " + 
                "ORDER BY p.updated ASC");
        
        rs = stmt.executeQuery();
        while(rs.next()) {
            Book book = new Book();
            book.setId(rs.getString("id"));
            book.setType(rs.getString("book_type"));
            book.setCono(rs.getString("cono"));
            book.setCustno(rs.getString("custno"));
            book.setShipto(rs.getString("ship_to"));
            book.setWhse(rs.getString("territory"));
            book.setEmailaddr(rs.getString("emailaddr"));
            book.setDelete(rs.getBoolean("remove"));
            book.setMakePdf(rs.getBoolean("makepdf"));
            book.setBookName(rs.getString("book_name"));
            book.setCustName(rs.getString("cust_name"));
            book.setQueueTime(rs.getDate("updated"));
            bookList.add(book);
        }        

        rs.close();
        stmt.close();

        return bookList;
    }    

    /**
     * 
     * @param b
     * @param status
     * @throws SQLException 
     */
    public synchronized void updateStatus(Book b, String status) throws SQLException
    {
        if(conn == null)
            throw new SQLException("java.sql.SQLException is NULL");

        PreparedStatement stmt = null;
        
        if(b.isDelete() == true) {
            stmt = conn.prepareStatement("UPDATE hive.dbo.pricebooks SET deleted=1 WHERE id=?");
            stmt.setString(1, b.getId());
            stmt.executeUpdate();
        }else{
            if(status.equals("Running"))
                stmt = conn.prepareStatement("UPDATE hive.dbo.pricebooks SET status=? WHERE id=?");
            else
                stmt = conn.prepareStatement("UPDATE hive.dbo.pricebooks SET status=?, pid=NULL WHERE id=?");
            stmt.setString(1, status);
            stmt.setString(2, b.getId());
            stmt.executeUpdate();
        }
        
        stmt.close();
    }    

    /**
     * 
     * @param cono
     * @param custno
     * @return
     * @throws SQLException 
     */
    public List<RetailGroup> getRetailItemGroups(String cono, String custno) throws SQLException
    {
        if(conn == null)
            throw new SQLException("java.sql.SQLException is NULL");

        int _cono = 1;
        try { _cono = Integer.parseInt(cono); } catch(Exception ex) {}
        
        List<RetailGroup> l = new ArrayList<RetailGroup>();

        PreparedStatement stmt = null;
        ResultSet rs = null;

        stmt = conn.prepareStatement
        (
            "SELECT p.uuid, p.tax_rate, pg.item_group, ig.description, pg.gm_pct, pg.labor_amt, pg.misc_amt, pg.comm_rate " +
            "FROM hive.dbo.pricebookRetailProfiles p " +
            "LEFT JOIN hive.dbo.pricebookRetailProfileGroups pg ON (pg.profile_id = p.uuid) " +
            "INNER JOIN hive.dbo.pricebookItemGroups ig ON (ig.cono=? AND ig.item_group = pg.item_group) " +
            "WHERE p.cono=? AND p.custno=? and p.active_profile=1"

        );
        stmt.setInt(1, _cono);
        stmt.setInt(2, _cono);
        stmt.setString(3,custno);
        
        rs = stmt.executeQuery();
        while(rs.next()) {
            RetailGroup g = new RetailGroup();
            g.setId(rs.getString("uuid"));
            g.setName(rs.getString("item_group"));
            g.setDescrip(rs.getString("description"));
            g.setGmPct(rs.getDouble("gm_pct"));
            g.setTaxRate(rs.getDouble("tax_rate"));
            g.setLaborCost(rs.getDouble("labor_amt"));
            g.setMiscCost(rs.getDouble("misc_amt"));
            g.setCommissionRate(rs.getDouble("comm_rate"));
            
            l.add(g);
        }
        
        rs.close();
        stmt.close();

        return l;
    }    
    
    /**
     * Converts a hex string to a byte array
     * 
     * @param s
     * @return 
     */
    public synchronized static byte[] hexStringToByteArray(String s) {
        if(s.startsWith(("0x")))
            s = s.substring(2);
        
        int len = s.length();
        byte[] data = new byte[len / 2];
        for (int i = 0; i < len; i += 2) {
            data[i / 2] = (byte) ((Character.digit(s.charAt(i), 16) << 4) + Character.digit(s.charAt(i+1), 16));
        }
        
        return data;
    }    

    /**
     * Strips slashes added by php during db insert
     * 
     * @param bytes
     * @return 
     */
    public byte[] stripPhpSlashes(byte[] bytes)
    {
        byte[] tmp = new byte[bytes.length];
        int tmpPos = 0;
        
        int v;
        for ( int i = 0; i < bytes.length; i++ ) {
            v = bytes[i] & 0xFF;

            //found a slash in current array position
            if(v == 92) {
                byte bt = bytes[i+1];
                switch(bt)
                {
                    case 10: //line feed
                    case 13: //carriage return    
                    case 26: //ctrl z
                    case 34: //double quote
                    case 39: //single quote
                    case 92: //backslash
                        tmp[tmpPos++] = bt;
                        i++;
                        break;                        
                    case 48: //null
                        tmp[tmpPos++] = 0;
                        i++;
                        break;
                    default: 
                        tmp[tmpPos++] = bytes[i];
                }
            }else{
                tmp[tmpPos++] = bytes[i];
            }
        }
        
        byte[] res = new byte[tmpPos];
        System.arraycopy(tmp, 0, res, 0, tmpPos);
    
        return res;
    }
    
    /**
     * Convert byte array to hex string
     * 
     * @param bytes
     * @return 
     */
    public String bytesToHex(byte[] bytes)
    {
        char[] hexArray = "0123456789ABCDEF".toCharArray();
        char[] hexChars = new char[bytes.length * 2];
        int v;
        for ( int j = 0; j < bytes.length; j++ ) {
            v = bytes[j] & 0xFF;

            hexChars[j * 2] = hexArray[v >>> 4];
            hexChars[j * 2 + 1] = hexArray[v & 0x0F];
        }
        
        return new String(hexChars);
    }    
    
    /**
     * Convert hex string to byte array
     * 
     * @param str
     * @return 
     */
    public byte[] hexToBytes(String str)
    {
        if ((str.length() % 2) == 1) str = "0" + str; // pad leading 0 if needed
        byte[] buf = new byte[str.length() / 2];
        int i = 0;

        for (char c : str.toCharArray())
        {
            byte b = Byte.parseByte(String.valueOf(c), 16);
            buf[i/2] |= (b << (((i % 2) == 0) ? 4 : 0));
            i++;
        }

        return buf;
    }
}