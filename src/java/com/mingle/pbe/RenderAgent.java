/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mingle.pbe;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.apache.log4j.Logger;

import com.aspose.cells.Cell;
import com.aspose.cells.Cells;
import com.aspose.cells.CellsHelper;
import com.aspose.cells.FileFormatType;
import com.aspose.cells.License;
import com.aspose.cells.SaveOptions;
import com.aspose.cells.Workbook;
import com.aspose.cells.Worksheet;
import com.aspose.cells.WorksheetCollection;
import com.lowagie.text.Document;
import com.lowagie.text.PageSize;
import com.lowagie.text.Rectangle;
import com.lowagie.text.pdf.BaseFont;
import com.lowagie.text.pdf.PdfContentByte;
import com.lowagie.text.pdf.PdfImportedPage;
import com.lowagie.text.pdf.PdfReader;
import com.lowagie.text.pdf.PdfWriter;
import com.mingle.pbe.beans.Agent;
import com.mingle.pbe.beans.Book;
import com.mingle.pbe.beans.Item;
import com.mingle.pbe.beans.RetailGroup;
import com.mingle.pbe.beans.RetailGroupMap;
import com.mingle.pbe.beans.Template;
import com.mingle.pbe.sxapi.PricebookDAO;
import com.mingle.pbe.sxapi.ProductDAO;
import com.mingle.pbe.sxapi.SxAPIWorker;

/**
 *
 * @author chris.weaver
 */
public class RenderAgent implements Runnable
{
    private static final Logger log = Logger.getLogger( RenderAgent.class );
    private final String workPath = String.valueOf(Environment.getProperty("workPath"));

    Agent agent;
    private int id;

    private String url, urlFP;
    private int retryTimes, retrySleep;
    
    public RenderAgent(Agent agent, int id)
    {
        url = String.valueOf(Environment.getProperty("SXAPI.URL"));
        urlFP = String.valueOf(Environment.getProperty("SXAPI.FP.URL"));
        
        retryTimes = 7; //infor recommended default
        retrySleep = 6; //infor recommended default
        
        try {
            retryTimes = Integer.parseInt( String.valueOf( Environment.getProperty("SXAPI.RETRYTIMES") ) );
            retrySleep = Integer.parseInt( String.valueOf( Environment.getProperty("SXAPI.RETRYSLEEP") ) );
        }catch(Exception ex) {
            log.error("Failed to set SXAPI environment variables; using defaults");
        }

        this.agent = agent;
        this.id = id;
    }
    
    @Override
    public void run()
    {
        if(agent.isExit() == true) {
            log.info("Agent " + agent.getId() + " received request to shutdown");
            return;
        }
        
        if(agent.getState() == AgentState.Paused) {
            log.debug("Agent " + agent.getId() + " is paused.");
            return;
        }        
        
        try {
            agent.setLastCheck(Calendar.getInstance().getTime());
            
            log.debug("RenderAgent id: " + id + " running");
 
            long startTm = System.nanoTime();

            PricebookDAO pb = new PricebookDAO();
            Book b = pb.getQueuedBook();

            if(b == null) {
                log.debug("No waiting books to run");
                return;
            }
            
            log.debug("[" + b.getPID() + "] RenderAgent id: " + id + " running " + b.getCustno());
            agent.start(b);
         
            //if we are working with future pricing, tie agent thread to future pricing server
            ProductDAO productDAO = null;
            if(b.getType().equalsIgnoreCase("dealer_future") || b.getType().equalsIgnoreCase("retail_future")) {
                log.info("[" + b.getPID() + "] Binding ProductDAO to Future Pricing Server @ : " + urlFP);
                productDAO = new ProductDAO( new SxAPIWorker(urlFP, retryTimes, retrySleep) );
            }else{
                log.info("[" + b.getPID() + "] Binding ProductDAO to Production Server @ : " + url);
                productDAO = new ProductDAO( new SxAPIWorker(url, retryTimes, retrySleep) );
            }
//---            
            //fetch book data
            try {
                if(pb.loadTemplates(b) == false) {
                    log.error("[" + b.getPID() + "] Load templates failed for: " + b.getCustno());
                }
            }catch(Exception pbe) {
                log.error("[" + b.getPID() + "] " + pbe);
            }
            
            //aspose - load license
            try {
                InputStream is = Thread.currentThread().getContextClassLoader().getResourceAsStream("/Aspose.Cells.lic");
                License lic = new License();
                lic.setLicense(is);            
            }catch(Exception ex) {
                log.error("[" + b.getPID() + "] Unable to load Aspose License. " + ex);
            }
            
            List<String> pdfList = new ArrayList<String>();
            List<String> finalList = new ArrayList<String>();
            
            //retail book - load customer price groups
            RetailGroupMap retailGroupMap = null;
            if(b.getType().equalsIgnoreCase("retail")) {
                List<RetailGroup> retailGroups = pb.getRetailItemGroups(b.getCono(), b.getCustno());
                retailGroupMap = new RetailGroupMap(retailGroups);
            }
            
            log.debug("[" + b.getPID() + "] begin template processing");
            
            //process each template
            List<Template> templates = b.getTemplates();
            agent.setNumTemplates(templates.size());
            for(int pos=0; pos<templates.size(); pos++)
            {
                Template t = templates.get(pos);
                agent.setCurrPos(pos+1);
                
                //interrupt thread if requested from Executor
                if(agent.getState() == AgentState.Interrupt) {
                    pb.updateStatus(b, "Kill");
                    agent.finish();
                    log.info("Agent " + id + " interrupt");
                    return;
                }
                
                log.debug("[" + b.getPID() + "] begin processing template: " + t.getName());
                
                if(String.valueOf(Environment.getProperty("BASE.OS")).equals("linux")) {
                    CellsHelper.setFontDir(String.valueOf(Environment.getProperty("ASPOSE.FONTDIR")));
                    log.info("[" + b.getPID() + "] Font dir set: " + String.valueOf(Environment.getProperty("ASPOSE.FONTDIR")));
                }
                
                Workbook workbook = new Workbook(workPath + b.getPID() + "_" + t.getId() + ".xls");
                log.debug("[" + b.getPID() + "] setting save options");
                SaveOptions so = workbook.getSaveOptions();
                so.setCachedFileFolder(workPath);
                
                log.debug("[" + b.getPID() + "] verify save options");
                log.debug("[" + b.getPID() + "] CachedFileFolder: " + workbook.getSaveOptions().getCachedFileFolder());
                
                log.info("[" + b.getPID() + "] begin processWorkbook()");
                processWorkbook(workbook, b.getCono(), b.getCustno(), b.getShipto(), b.getWhse(), b.getType(), retailGroupMap, productDAO);
                log.debug("[" + b.getPID() + "] end processWorkbook()");
                
                log.debug("[" + b.getPID() + "] begin calculations");
                workbook.calculateFormula();
                log.debug("[" + b.getPID() + "] end calculations");

                String filename = "";
                if(b.isMakePdf() == true) {
                    log.debug("[" + b.getPID() + "] begin save as pdf");
                    filename = b.getPID()+ "_" + b.getCustno() + "_" + t.getId() + ".pdf";
                    workbook.save(workPath + filename, FileFormatType.PDF);
                    pdfList.add(workPath + filename);
                    log.debug("[" + b.getPID() + "] end save as pdf");
                }else{
                    log.debug("[" + b.getPID() + "] begin save as xls");
                    filename = b.getPID()+ "_" + b.getCustno() + "_" + t.getId() + ".xlsx";
                    workbook.save(workPath + filename, FileFormatType.XLSX);
                    finalList.add(filename);
                    log.debug("[" + b.getPID() + "] end save as xls");
                }

                t.setFilename(filename);
                
                log.debug("[" + b.getPID() + "] end processing template: " + t.getName());
            }

            log.debug("[" + b.getPID() + "] end template processing");
            
            //merge if pdf
            if(b.isMakePdf() == true) {
                log.debug("[" + b.getPID() + "] begin pdf merge");
                String finalPdf = mergePDFs(pdfList, b, true);
                finalList.add(finalPdf);
                log.debug("[" + b.getPID() + "] end pdf merge");
            }

            //email
            log.debug("[" + b.getPID() + "] begin send email");
            sendEmail(b, finalList);
            log.debug("[" + b.getPID() + "] end send email");
            
            //cleanup
            for(Template t : b.getTemplates()) {
                try {
                    new File(workPath + t.getFilename()).delete();
                    new File(workPath + b.getPID() + "_" + t.getId() + ".xls").delete();
                }catch(Exception ex) {
                    log.error(ex);
                }
            }
            try {
                for( String file : finalList )
                    new File(workPath + file).delete();
            }catch(Exception ex) {
                log.error(ex);
            }
//---            
            //update book status
            pb.updateStatus(b, "Ready");            
            
            //dump stats
            DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
            Calendar cal = Calendar.getInstance();
            long runTime = (System.nanoTime() - startTm) / 1000000;
            log.info("[" + b.getPID() + "] RenderAgent id: " + id + "; completed in " + runTime + "ms; " + productDAO.getCacheHits() + " cache hits; finished at: " + dateFormat.format(cal.getTime()));
            
            agent.finish();
            
        } catch (InterruptedException ex) {
            log.error(ex);
        } catch (Exception ex) {
            log.error(ex);
        }
    }
    
    /**
     * 
     * @param b
     * @param files
     * @throws Exception 
     */
    public void sendEmail(Book b, List<String> files) throws Exception
    {
        String newLine = System.getProperty("line.separator");
                
        Emailer emailer = new Emailer("smtp-relay.mingledorffs.com");
        emailer.setFromAddress("pricebooks@mingledorffs.com");
        emailer.addToAddress(b.getEmailaddr());
        emailer.setSubject("Your pricebook ready");
        emailer.setMessage("Here is the pricebook you requested." + newLine + newLine);

        emailer.appendMessage("Customer #: " + b.getCustno() + newLine);
        if(b.getShipto() != null && b.getShipto().length() > 0) 
            emailer.appendMessage("Ship To: " + b.getShipto() + newLine);
        emailer.appendMessage("Warehouse: " + b.getWhse() + newLine + newLine);

        //if(errors.size() > 0) {
        //    emailer.appendMessage(errors.size() + " Errors encountered while processing:\n");
        //    for(String error : errors) {
        //        emailer.appendMessage(error + "\n");
        //    }
        //}

        for( String file : files )
            emailer.addAttachment(workPath + file);
        
        emailer.sendMessage(false);        
    }
    
    /**
     * 
     * @param workbook
     * @param cono
     * @param custno
     * @param whse 
     */
    private void processWorkbook(Workbook workbook, String cono, String custno, String shipto, String whse, String type, RetailGroupMap retailGroupMap, ProductDAO productDAO)
    {
        WorksheetCollection worksheets = workbook.getWorksheets();
        Cell cell;
        String value;

        //worksheets
        for(int i=0; i<worksheets.getCount(); i++) {
            
            Worksheet worksheet = worksheets.get(i);

            Cells cells = worksheet.getCells();

            //rows
            for(int r=0; r<=cells.getMaxDataRow(); r++) {
                
                //cols
                for(int c=0; c<=cells.getMaxDataColumn(); c++) {

                    if(agent.getState() == AgentState.Interrupt) { return; }

                    cell = cells.get(r, c);
                    value = String.valueOf(cell.getValue());

                    //fetch price
                    if(value.contains("[PRICE:")) {
                        try {
                            int colPos = Utilities.getColPosition(value.substring(7, value.length()-1));
                            String prod = (String) cells.get(r,colPos).getValue();
                            Item item = productDAO.getItemPrice(cono, custno, shipto, prod, whse);
                            
                            if(prod.equals("24ABB318A003"))
                                log.info("price: " + prod + " -> " + item.price);
                            
                            //retail book
                            if(type.equalsIgnoreCase("retail") || type.equalsIgnoreCase("retail_future"))
                            {
                                //calculate retail price
                                RetailGroup retailGroup = retailGroupMap.find(prod);
                                BigDecimal retail = calcRetailPrice(retailGroup, item.price);

                                if(retail.equals(BigDecimal.ZERO))
                                    cell.setFormula("");
                                else
                                    cell.setValue(retail);

                                //if(retail != 0.00 && (item.getGmPct() == 0 || item.getLaborCost() == 0 || item.getMiscCost() == 0)) {
                                //    XPropertySet prop = (XPropertySet) UnoRuntime.queryInterface(XPropertySet.class, xCell);
                                //    prop.setPropertyValue("CellBackColor", AnyConverter.toInt(0xFFFF00));
                                //}                                
                            }else{
                                cell.setValue(item.price);
                            }

                        }catch(Exception ex) {
                            try {
                                int colPos = Utilities.getColPosition(value.substring(7, value.length()-1));
                                 String prod = (String) cells.get(r,colPos).getValue();
                                 cell.setValue("CALL");
                            }catch(Exception ex1) {
                                cell.setValue("ERROR");
                            }
                        }
                    }
                }
            }
        }
    }
    
    /**
     * 
     * @param prod
     * @param wholesale
     * @return
     */
    private BigDecimal calcRetailPrice(RetailGroup item, BigDecimal wholesale)
    {
        BigDecimal retailPrice = BigDecimal.ZERO;
        
        //need to convert all over to BigDecimal
        double whsePrice = wholesale.doubleValue();
        
        if(item != null && item.getGmPct() > 0  )
        {
            double cost = ((whsePrice + item.getMiscCost()) * (1 + (item.getTaxRate() / 100))) + item.getLaborCost();
            double price = (cost / (1 - (item.getGmPct() / 100))) * (1 + (item.getCommissionRate() / 100));

            retailPrice = new BigDecimal(price);

        }else if(item != null && item.getGmPct() == 0) {
            double cost = ((whsePrice + item.getMiscCost()) * (1 + (item.getTaxRate() / 100))) + item.getLaborCost();
            double price = cost * (1 + (item.getCommissionRate() / 100));

            retailPrice = new BigDecimal(price);
            //return Math.round(price);
        }

        return retailPrice;
    }    
    

    
    /**
     * 
     * @param inputPDFs
     * @param book
     * @param paginate
     * @return
     */
    public String mergePDFs(List<String> inputPDFs, Book book, boolean paginate)
    {
        Document document = null;
        OutputStream outputStream = null;

        String outputFile = book.getCustno() + "_" + book.getPID() + ".pdf";

        try {
            SimpleDateFormat df = new SimpleDateFormat("MM/dd/yyyy");
            String date = df.format(new Date());

            document = new Document(PageSize.LETTER);

            outputStream = new FileOutputStream(workPath + outputFile);

            List<PdfReader> readers = new ArrayList<PdfReader>();
            int totalPages = 0;

            // Create Readers for the pdfs.
            for(String file : inputPDFs)
            {
                InputStream pdf = new FileInputStream(file);
                PdfReader pdfReader = new PdfReader(pdf);
                readers.add(pdfReader);
                totalPages += pdfReader.getNumberOfPages();
                pdf.close();
            }
            
            // Create a writer for the outputstream
            PdfWriter writer = PdfWriter.getInstance(document, outputStream);

            document.open();
            BaseFont bf = BaseFont.createFont(BaseFont.HELVETICA, BaseFont.CP1252, BaseFont.NOT_EMBEDDED);

            // Holds the PDF
            PdfContentByte cb = writer.getDirectContent();

            PdfImportedPage page;
            int currentPageNumber = 0;
            int pageOfCurrentReaderPDF = 0;

            Iterator<PdfReader> iteratorPDFReader = readers.iterator();

            // Loop through the PDF files and add to the output.
            while (iteratorPDFReader.hasNext())
            {
                PdfReader pdfReader = iteratorPDFReader.next();
                Rectangle rec = pdfReader.getPageSizeWithRotation(1);

                // Create a new page in the target for each source page.
                while (pageOfCurrentReaderPDF < pdfReader.getNumberOfPages())
                {
                    int centerPos = (int) rec.getWidth() / 2;
                    int rightPos = (int) rec.getWidth() - 20;

                    //set portrait or landscape
                    if(rec.getHeight() < rec.getWidth()) {
                        document.setPageSize(PageSize.LETTER.rotate());
                    } else {
                        document.setPageSize(PageSize.LETTER);
                    }

                    document.newPage();

                    pageOfCurrentReaderPDF++;
                    currentPageNumber++;
                    page = writer.getImportedPage(pdfReader, pageOfCurrentReaderPDF);
                    cb.addTemplate(page, 0, 0);
                    
                    // Code for pagination
                    if (paginate) {
                        cb.beginText();
                        cb.setFontAndSize(bf, 8);
                        cb.showTextAligned(PdfContentByte.ALIGN_LEFT, "Prices subject to change without notice", 20, 20, 0);
                        cb.showTextAligned(PdfContentByte.ALIGN_LEFT, "This information is private and confidential", 20, 10, 0);
                        cb.showTextAligned(PdfContentByte.ALIGN_CENTER, "" + currentPageNumber + " of " + totalPages, centerPos, 10, 0); //300
                        cb.showTextAligned(PdfContentByte.ALIGN_RIGHT, date, rightPos, 30, 0);
                        cb.showTextAligned(PdfContentByte.ALIGN_RIGHT, book.getCustName(), rightPos, 20, 0); //580
                        cb.showTextAligned(PdfContentByte.ALIGN_RIGHT, book.getBookName(), rightPos, 10, 0); //580
                        cb.endText();
                    }
                }
                pageOfCurrentReaderPDF = 0;
                pdfReader.close();
            }

            outputStream.flush();
            document.close();
            outputStream.close();

        } catch (Exception e) {
            log.error(e);
        } finally {
            if (document.isOpen())
                document.close();
            try {
                if (outputStream != null)
                outputStream.close();
            } catch (IOException ioe) {
                log.error(ioe);
            }
        }

        return outputFile;
    }  
}