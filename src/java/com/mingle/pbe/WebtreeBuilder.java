/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mingle.pbe;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.aspose.cells.Cell;
import com.aspose.cells.Cells;
import com.aspose.cells.Workbook;
import com.aspose.cells.Worksheet;
import com.aspose.cells.WorksheetCollection;
import com.mingle.pbe.beans.Template;
import com.mingle.pbe.sxapi.PricebookDAO;

/**
 *
 * @author chris.weaver
 */
public class WebtreeBuilder implements Runnable
{
    private static final Logger log = Logger.getLogger( WebtreeBuilder.class );
    private final String workPath = String.valueOf(Environment.getProperty("workPath"));
    public static boolean verifyFailed = false;
    public static List<String[]> failures ;
    
    @Override
    public void run()
    {
        log.info("run");
        
        try {
            PricebookDAO pb = new PricebookDAO();

            List<Template> templates = pb.loadWebTreeTemplates();
            log.info("after templates");

            List<String[]> results = new ArrayList<String[]>();
          //Initialize the data verification flags.
            failures = new ArrayList<String[]>();
            verifyFailed = false;
            
            //loop through templates
            for(Template template : templates )
            {
                System.out.println(template.getName());
                try { 
                    Workbook workbook = new Workbook(workPath + template.getId() + ".xls");
                    results.addAll( getTreeData(workbook, template) );
                }catch(Exception ex1) {
                    System.out.println(template.getName()+ " " + ex1.getMessage());
                    throw new Exception(template.getName()+ " " + ex1.getMessage());
                }
                
                log.info("after getTreeData");
            }
            log.info("total no of records:" + results.size());
            
            //check Data verification status
			log.info("is Verification failed?:"+verifyFailed);
			if(verifyFailed){
				//Send email with failed info.
				sendEmail(failures);
				log.info("Email sent with failed records");
			}
			
            //clear table
            pb.clearWebTree();
            log.info("after clear");

            //insert record
            for(String[] line : results) {
            	//printArray(line);
                pb.insertWebTreeItem(line);
            	
            }            
            log.info("after insert");        
        
        }catch(Exception ex) {
            log.error(ex);
            System.out.println("error"+ex.getMessage());
        }
    }
    
    private static void printArray(String[] anArray) {
        for (int i = 0; i < anArray.length; i++) {
           if (i > 0) {
              System.out.print(", ");
           }
           System.out.print(anArray[i]);
        }
     }
    
    private List<String[]> getTreeData(Workbook workbook, Template template)
    {
        WorksheetCollection worksheets = workbook.getWorksheets();
        Cell cell;
        String value;

        List<String[]> results = new ArrayList<String[]>();
        //log.info("count:"+worksheets.getCount());
        //worksheets
        for(int i=0; i<worksheets.getCount(); i++) {
            
            Worksheet worksheet = worksheets.get(i);

            Cells cells = worksheet.getCells();

            //rows
            log.info("start loading the tree data...");
            for(int r=0; r<=cells.getMaxDataRow(); r++) {
                
                //cols
                for(int c=0; c<=cells.getMaxDataColumn(); c++) {
                    try {
                        cell = cells.get(r, c);
                        value = String.valueOf(cell.getValue());
                        //log.info("value:"+value);
                        if(value.contains("[PRICE:")) {
                            int colPos = Utilities.getColPosition(value.substring(7, value.length()-1));
                            String prod = (String) cells.get(r,colPos).getValue();                        

                            String[] rec = new String[5];
                            rec[0] = template.getId();
                            rec[1] = template.getName();
                            rec[2] = worksheet.getName();
                            rec[3] = prod;

                            //if column to the right is a sort tag, get sort value
                            cell = cells.get(r, c+1);
                            value = String.valueOf(cell.getValue());
                            if(value.contains("[SORT:"))
                                rec[4] = value.substring(6, value.length()-1);
                            
                            // Verify data against the table "productPricebookItems" column length.
                            try {
                            	if(verifyRecord(rec)) 
                            		results.add(rec);
                            } catch(Exception e){
                            log.error("Data verification failed - " + template.getName() + ": " + e);
                            //Verification completed.
                            }
                            results.add(rec);
                        }  
                    }catch(Exception ex) {
                        log.error(template.getName() + ": " + ex);
                    }
                }
            }
            log.info("loading tree data completed ...");
        }
        log.info("total for:"+template.getName() +" : " + results.size());
        return results;
    }
    
  //Verify data against the table "productPricebookItems" column length.
    private static boolean verifyRecord(String[] record) {
    	
        int tIdLength = 36;
        int tNameLength = 100;
        int shNameLength = 50;
        int prodLength = 50;
        boolean validRecord = true;
        
        try {
        	tIdLength = Integer.parseInt( String.valueOf( Environment.getProperty("DATA.VERIFICATION.TEMPLATE.ID") ) );
        	tNameLength = Integer.parseInt( String.valueOf( Environment.getProperty("DATA.VERIFICATION.TEMPLATE.NAME") ) );
        	shNameLength = Integer.parseInt( String.valueOf( Environment.getProperty("DATA.VERIFICATION.SHEET.NAME") ) );
        	prodLength = Integer.parseInt( String.valueOf( Environment.getProperty("DATA.VERIFICATION.PROD") ) );

    		if(record.length >=4) {
    			if((null != record[0] && record[0].length() > tIdLength) ||
    					(null != record[1] && record[1].length() > tNameLength) ||
    					(null != record[2] && record[2].length() > shNameLength) ||
    					(null != record[3] && record[3].length() > prodLength)) {
    				failures.add(record);
    				validRecord = false;
    				verifyFailed = true;
    			}
    		}
        } catch(Exception e){
        	log.error("Data verification failed - " + e);
        	validRecord = false;
        }
        //Verification completed.
        return validRecord;
     }
    
    public void sendEmail(List<String[]> records) throws Exception
    {
        String newLine = System.getProperty("line.separator");
                
        Emailer emailer = new Emailer("smtp-relay.mingledorffs.com");
        emailer.setFromAddress("pricebooks@mingledorffs.com");
        
        emailer.addToAddress("jyockel@mingledorffs.com");
        emailer.addToAddress("rmassey@mingledorffs.com");
        emailer.addCcAddress("nchatrathi@mingledorffs.com");
        
        emailer.setSubject("Need Attention: PriceBook-Data Verification failed");
        emailer.setMessage("Below is the list of records needs to be revisited." + newLine + newLine);
        log.info("failed records size:"+records.size());
        for(String[] record : records) {
        	emailer.appendMessage("Template ID#: " + record[0] + ", Template Name#: " + record[1] + ", Sheet Name#: " + record[2] + ", Prod #: " + record[3] + newLine);
        }
        log.info("final email msg:"+emailer.getMessage());
        emailer.sendMessage(false);        
    }
    
}