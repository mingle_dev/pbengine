/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mingle.pbe;

import java.util.Properties;
import org.apache.log4j.Logger;

/**
 *
 * @author chris.weaver
 */
public class Environment
{
    private static final Properties properties = new Properties();
    private static final Logger log = Logger.getLogger( Environment.class );
    
    static
    {
        try {
            properties.load(Thread.currentThread().getContextClassLoader().getResourceAsStream("/environment.properties"));     
        }catch(Exception ex) {
            log.error("Loading Environment Variables Failed: " + ex);
        }
        
        String os = System.getProperty("os.name", "generic").toLowerCase();
        log.info(os);
        if (os.indexOf("win") >= 0) {
            properties.setProperty("BASE.OS", "win");
            properties.setProperty("workPath", "c:/temp/");
        }else{
            properties.setProperty("BASE.OS", "linux");
            properties.setProperty("workPath", "/tmp/");
        }
        log.info("Environment Variables Loaded");
    }
    
    /**
     * 
     * @param key
     * @return 
     */
    public static Object getProperty(String key)
    {
        return properties.get(key);
    }
    
    /**
     * 
     * @param key
     * @param value 
     */
    public static void setProperty(String key, String value)
    {
        properties.put(key, value);
    }
}