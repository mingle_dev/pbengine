<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE HTML>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta http-equiv="refresh" content="5">
        <title>PBEngine Console</title>
    </head>

    <body>
        <h2>PBEngine Status</h2>
        Current Time: ${msg}<br/><br/>
        <table border="1">
            <tr>
                <td width="75" style="text-align: center;">Agent Id</td>
                <td width="80" style="text-align: center;">State</td>
                <td width="125" style="text-align: center;">Last Check In</td>
                <td width="75" style="text-align: center;">PID</td>
                <td width="100">Customer #</td>
                <td width="200">Book Name</td>
                <td width="75" style="text-align: center;">Completed</td>
                <td width="200">Start Time</td>
            </tr>
            <c:forEach var="agent" items="${agents}">
            <tr>
                <td style="text-align: center;">${agent.id}</td>
                <td style="text-align: center;">
                    <c:choose>
                        <c:when test="${agent.state == 'Paused'}">
                            <a href='pause.htm?agent_id=<c:out value="${agent.id}"/>'>${agent.state}</a>
                        </c:when>
                        <c:when test="${agent.state == 'Running'}">
                            ${agent.state}
                        </c:when>
                        <c:otherwise>
                            <a href='pause.htm?agent_id=<c:out value="${agent.id}"/>'>${agent.state}</a>
                        </c:otherwise>
                    </c:choose>                    
                </td>
                <td style="text-align: center;">
                    <fmt:formatDate pattern="HH:mm:ss.SSSS" value="${agent.lastCheck}" />
                </td>
                <td style="text-align: center;">${agent.PID}</td>
                <td>${agent.custNo}</td>
                <td>${agent.bookName}</td>
                <td style="text-align: center;">
                    <c:if test="${agent.PID != ''}">
                        <fmt:formatNumber value="${(agent.currPos / agent.numTemplates) * 100}" maxFractionDigits="0" /> %
                    </c:if>
                </td>
                <td><fmt:formatDate type="both" value="${agent.startTm}" /></td>
            </tr>
            </c:forEach>
        </table>  
        
        <h3>In Queue</h3>
        <table border="1">
            <tr>
                <td width="100">Customer #</td>
                <td width="200">Book Name</td>
                <td width="250">Queued Time</td>
            </tr>
            <c:forEach var="book" items="${queuedBooks}">
            <tr>
                <td width="100">${book.custno}</td>
                <td width="200">${book.bookName}</td>
                <td width="250"><fmt:formatDate type="both" value="${book.queueTime}" /></td>
            </tr>
            </c:forEach>
        </table> 
        
    </body>
</html>